const hamburger = document.querySelector('.header .nav-bar .nav-list .hamburger');
const mobile_menu = document.querySelector('.header .nav-bar .nav-list ul');
const menu_item = document.querySelectorAll('.header .nav-bar .nav-list ul li a');
const header = document.querySelector('.header.container');


$(document).ready(function () {
  $(".nav-lists li").hover(function () {
    $(".menu_on_hover").fadeIn(400)
  });
  $(".menu_on_hover").mouseleave(function () {
    $(".menu_on_hover").fadeOut(400)
  });
});

$(window).scroll(function(event){ 
  if ($(window).scrollTop() != 0) {
    $('.menu_on_hover').fadeOut(100);
  }
})

$(window).scroll(function(event){ 
  if ($(window).scrollTop() > 0) {
    $('.header-top').addClass('fixed');
    $('.header-top').addClass('up-menu');
  }
  else {
    $('.header-top').removeClass('fixed');
    $('.header-top').removeClass('up-menu');
  }
})

// ------------------------------
$(window).bind('scroll', function() {
     if ($(window).scrollTop() > 0) {
         $('.header-top2').addClass('fixed');
         $('.header-top2').addClass('up-menu');
     }
     else {
         $('.header-top2').removeClass('fixed');
         $('.header-top2').removeClass('up-menu');
     }
});

$(document).ready(function () {
  $(".nav-lists li").hover(function () {
    $(".menu_on_hover2").fadeIn(400)
  });
  $(".menu_on_hover2").mouseleave(function () {
    $(".menu_on_hover2").fadeOut(400)
  });
});

$(window).scroll(function(event){ 
  if ($(window).scrollTop() != 0) {
    $('.menu_on_hover2').fadeOut(100);
  }
})

// ------------------------------
$('.slider').slick({
   dots: false,
   speed:2000,
   slidesToShow: 5,
   slidesToScroll: 5,
   arrows:true,
   nextArrow:'<button type="button" class="slick-next"></button>',
   responsive: [
     {
       breakpoint: 1024,
       settings: {
         slidesToShow: 3,
         slidesToScroll: 3,
         infinite: true,
         dots: true
       }
     },
     {
       breakpoint: 600,
       settings: {
         slidesToShow: 2,
         slidesToScroll: 2
       }
     },
     {
       breakpoint: 480,
       settings: {
         slidesToShow: 1,
         slidesToScroll: 1
       }
     }
   ]
 });

 $('.slider_mobile').slick({
  dots: false,
  speed:1000,
  slidesToShow: 4,
  slidesToScroll: 2,
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3,
        infinite: true,
        dots: true
      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
  ]
});