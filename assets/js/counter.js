// Selector
const counters = document.querySelectorAll('.timer');
// Main function
for(let n of counters) {
  const updateCount = () => {
    const target = + n.getAttribute('data-to');
    const count = + n.innerText;
    const speed = n.getAttribute('data-speed'); // change animation speed here
    const inc = target / speed; 
    if(count < target) {
      n.innerText = Math.ceil(count + inc);
      setTimeout(updateCount, 1);
    } else {
      n.innerText = target;
    }
  }
  updateCount();
}